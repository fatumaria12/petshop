﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Petshop2.Models
{
    public class Pet
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public virtual Store Store { get; set; }
        public int StoreId { get; set; }
        public Person Person { get; set; }
        public int PersonId { get; set; }

        public IEnumerable<Transaction> Transactions { get; set; }

    }
}
