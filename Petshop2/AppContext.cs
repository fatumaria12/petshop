﻿using Microsoft.EntityFrameworkCore;
using Petshop2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Petshop2
{
    public class AppContext:DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Pet> Pets { get; set; }
        public DbSet<Store> Stores { get; set; }

        public AppContext(DbContextOptions<AppContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Pet>().HasKey(c => new { c.StoreId, c.PersonId });
            modelBuilder.Entity<Transaction>().HasKey(c => new { c.PersonId, c.PetId });
        }

    }
}
