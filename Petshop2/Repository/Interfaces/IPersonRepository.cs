﻿using Petshop2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Petshop2.Repository
{
    public interface IPersonRepository
    {
        Person GetPeople(int id);
        IEnumerable<Person> GetPeople();
        void AddPerson(Person p);
        void RemovePerson(int p);
        void UpdatePerson(Person p);
    }
}
