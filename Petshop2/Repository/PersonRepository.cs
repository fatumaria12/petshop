﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Petshop2.Models;

namespace Petshop2.Repository
{
    public class PersonRepository : IPersonRepository
    {
        private readonly AppContext _context;
        public DbSet<Person> People;

        public PersonRepository(AppContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(AppContext));
            People = context.Set<Person>();
        }
        
        public void AddPerson(Person p)
        {
            People.Add(p);
            _context.SaveChanges();
        }

        public IEnumerable<Person> GetPeople()
        {
            return People.Include(c => c.Pet);
        }

        public Person GetPeople(int id)
        {
            return People.FirstOrDefault(c => c.Id == id); 
        }

        public void RemovePerson(int id)
        {
            var person= People.FirstOrDefault(c => c.Id == id);
            People.Remove(person);
            _context.SaveChanges();
        }

        public void UpdatePerson(Person p)
        {
            People.Update(p);
            _context.SaveChanges();
        }
    }
}
