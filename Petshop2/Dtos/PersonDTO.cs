﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetShop2.Dtos
{
    public class PersonDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
