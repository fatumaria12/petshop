﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Petshop2.Models;
using PetShop2.Dtos;

namespace Petshop2.Profiles
{
    public class PersonProfile:Profile
    {
        public PersonProfile()
        {
            CreateMap<Person, PersonDTO>().ReverseMap();
        }
    }
}
