﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Petshop2.Models;
using Petshop2.Repository;
using PetShop2.Dtos;

namespace Petshop2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        public IPersonRepository _personRepo;
        public IMapper _mapper;

        public ValuesController(IPersonRepository personRepo, IMapper mapper)
        {
            _personRepo = personRepo;
            _mapper = mapper;

        }
        // GET api/values
        [HttpGet]
        public IEnumerable<PersonDTO> Get()
        {
            var people = _personRepo.GetPeople().ToList();
            return _mapper.Map<List<PersonDTO>>(people);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public PersonDTO Get(int id)
        {
            var person = _personRepo.GetPeople(id);
            return _mapper.Map<PersonDTO>(person);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Person value)
        {
            _personRepo.AddPerson(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Person value)
        {
            _personRepo.UpdatePerson(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _personRepo.RemovePerson(id);
        }
    }
}
